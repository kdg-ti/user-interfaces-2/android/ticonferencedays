package be.kdg.ticonferencedays.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import be.kdg.ticonferencedays.R

class ConferenceInfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_conference_info)
    }
}

package be.kdg.ticonferencedays.model

import be.kdg.ticonferencedays.R
import java.time.LocalDate

fun getTestSessions():Array<Session> {
    return arrayOf(
        Session("The Actor Model", LocalDate.of(2017,2,16), R.drawable.session1),
        Session("Kotlin, a new hope", LocalDate.of(2017,2,16), R.drawable.session2),
        Session("Docker vs virtual machines", LocalDate.of(2017,2,16), R.drawable.session3),
        Session("Crowdfunding blockchained", LocalDate.of(2017,2,16), R.drawable.session4),
        Session("Datascience is knowledge", LocalDate.of(2017,2,16), R.drawable.session5),
        Session("Concurrent programming in Akka.Net", LocalDate.of(2017,2,16), R.drawable.session6),
        Session("Vulnerabilities", LocalDate.of(2017,2,16), R.drawable.session7),
        Session("Conversational User Interfaces", LocalDate.of(2017,2,16), R.drawable.session8),
        Session("Communicatie tussen microservices", LocalDate.of(2017,2,16), R.drawable.session9),
        Session("Best practice bij Big Data analytics", LocalDate.of(2017,2,16), R.drawable.session10)
    )
}
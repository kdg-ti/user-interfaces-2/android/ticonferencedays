package be.kdg.ticonferencedays.model

import java.time.LocalDate

data class Session(
  val title: String,
  val date: LocalDate,
  val imageId: Int
)